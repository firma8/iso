module ISO
  class Tag
    attr_reader :original, :language, :regions

    def initialize(tag)
      @original = tag
      @language = Language.new(tag.downcase)
      @regions   = Region.identify(@original) || UN::Region.identify(@original)
    end

    def name
      if self.language.name
        if self.regions.empty?
          self.language.name
        else
          region_names = self.regions.map do |e|
            self.language.default_regions[e] if self.language.default_regions && self.language.default_regions[e]
          end.compact
          "#{self.language.name}-#{region_names.empty? ? self.regions.join('-') : region_names.join('-')}"
        end
      else
        self.original
      end
    end

    def language_tag
      if self.language.alpha2
        if self.regions.empty?
          self.language.alpha2
        else
          "#{self.language.alpha2}-#{self.regions.join('-')}"
        end
      else
        @original
      end
    end

  end
end
