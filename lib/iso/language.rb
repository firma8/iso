module ISO
  class Language
    DEFINITIONS_FILE          = YAML.load_file("#{File.dirname(__FILE__)}/../../data/iso-639-3.yml")
    MAPPING_FILE              = YAML.load_file("#{File.dirname(__FILE__)}/../../data/iso_639_1_to_iso_639_3_mapping.yml")
    ALL_LANGUAGES             = "#{File.dirname(__FILE__)}/../../languages.yml"
    DEFAULT_PLURAL_RULE_NAMES = %w(one other)
    DEFAULT_DIRECTION         = 'ltr'
    DEFAULT_CODE              = 'en'
    PLURAL_RULE_NAMES         = %w(zero one two few many other)

    attr_reader :code, :code_attr

    def initialize(code, options={})
      @code = Language.convert_639_1_to_639_3 code.scan(/^[A-Za-z]{1,}/).first
      @code_attr = DEFINITIONS_FILE[@code]
    end

    def plural_rule_names
      @code_attr ? @code_attr["plural_rule_names"] : DEFAULT_PLURAL_RULE_NAMES
    end

    def direction
      @code_attr ? @code_attr["direction"] : DEFAULT_DIRECTION
    end

    def default_regions
      @code_attr ? @code_attr["default_regions"] : []
    end

    def name
      @code_attr ? @code_attr["name"] : nil
    end

    def alpha2
      @code_attr ? @code_attr["alpha2"] : nil
    end

    def self.all_tags
      tags = []
      DEFINITIONS_FILE.each do |lang, options|
        tags.push({name: options['name'], code: lang})
        if options["default_regions"]
          options["default_regions"].each do |key, region|
            tags.push({name: "#{options['name']}-#{region}", code: "#{lang}-#{key}"})
          end
        end
      end
      tags
    end

    def self.convert_639_1_to_639_3(code)
      MAPPING_FILE[code] || code
    end

    def self.identify(code, response={})
      DEFINITIONS_FILE[code.downcase].try(:each) do |key, value|
        response.merge! key.to_sym => value
      end
      response.merge!(code: code).has_key?(:name) ? response : nil
    end
    singleton_class.send(:alias_method, :find, :identify)
  end
end