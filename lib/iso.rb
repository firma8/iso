require 'i18n'
require 'yaml'
require 'active_support/dependencies/autoload'

I18n.load_path << Dir[File.join(File.expand_path(File.dirname(__FILE__) + '/../locales'), '*.yml')]
I18n.load_path.flatten!

module ISO
  extend ActiveSupport::Autoload

  autoload :Language
  autoload :Region
  autoload :Subtag
  autoload :Tag
end